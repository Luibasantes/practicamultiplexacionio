CC = gcc
CFLAGS = -O2 -Wall -I .

# This flag includes the Pthreads library on a Linux box.
# Others systems will probably require something different.
LIB = -lpthread -ludev

all: cliente monitor

cliente: cliente.c csapp.o
	$(CC) $(CFLAGS) -o cliente cliente.c csapp.o $(LIB)

monitor: monitor.c
	$(CC) $(CFLAGS) -o monitor monitor.c csapp.o $(LIB)

csapp.o: csapp.c
	$(CC) $(CFLAGS) -c csapp.c

clean:
	rm -f *.o cliente monitor *~

